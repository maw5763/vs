public class ImplFigur extends Figur {
    // without synchronize not only on diagonal
    public synchronized void setPosition(char x, int y){
        this.x = x;
        MachMal.eineZehntelSekundeLangGarNichts();
        this.y = y;
    }
    // without synchronize not only on diagonal
    public synchronized String getPosition(){
        try {
            Thread.sleep(1000);
        }
        catch (InterruptedException e) {
        }
        return "" + this.x + this.y;
    }
}
