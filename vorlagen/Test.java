import java.io.IOException;

public class Test {
    public static void main(String[] args){
        UniqueId datei = new UniqueId("id.dat");
        try {
            datei.init(10000);
        } catch (IOException e) {
            e.printStackTrace();
        }
        for(int i = 0; i < 5; i++){
            int finalI = i;
            new Thread(() -> {
                for(int j = 0; j < 10; j++){
                    try {
                        System.out.println(datei.getNext() + " " + finalI);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }
}
