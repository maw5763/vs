public class FigurThreads{
    public static void main(String[] args){
        Figur f = new ImplFigur();
        Leser l1 = new Leser(f);
        l1.start();
        Schreiber s1 = new Schreiber(f);
        s1.setDaemon(true);
        s1.start();
    }
}
