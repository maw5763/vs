import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DaytimeImpl extends UnicastRemoteObject implements Daytime{
    public DaytimeImpl() throws RemoteException {
        System.out.println("constructor");
    }
    @Override
    public String getTime() throws RemoteException {
        return "" + new SimpleDateFormat("yyyy-MM-dd_HH:mm:sss").format(Calendar.getInstance().getTime());
    }
}
