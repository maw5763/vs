import java.rmi.Naming;
import java.rmi.Remote;

public class DaytimeServer {
    public static void main(String args[]) throws
            Exception {
        System.out.print("Starting Daytime server...");
        Remote remote = new DaytimeImpl();
        Naming.rebind("daytime", remote);
        System.out.println("done!");
    }
}
