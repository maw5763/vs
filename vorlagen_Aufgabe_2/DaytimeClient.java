import java.rmi.Naming;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DaytimeClient {
    public static void main(String args[]) throws Exception {
        String host = args[0];
//        String text = args[1];
        Daytime remote = (Daytime) Naming.lookup("//" + host + "/daytime");
        System.out.println("Local starttime is " + new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss.sss").format(Calendar.getInstance().getTime()));
        String received = remote.getTime();
        System.out.println("Received time is " + received);
        System.out.println("Local endtime is "+ new SimpleDateFormat("yyyy-MM-dd_HH:mm:sss").format(Calendar.getInstance().getTime()));
    }
}
